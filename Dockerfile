from microsoft/aspnet:4.7.1-windowsservercore-ltsc2016

WORKDIR /inetpub/wwwroot

COPY dotnet471api .

RUN New-Website -Name 'dotnet471api' -Port 80 \
    -PhysicalPath '/inetpub/wwwroot' -ApplicationPool '.NET v4.7.1'

EXPOSE 80