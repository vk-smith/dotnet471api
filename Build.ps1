$ProjectName = "dotnet471api"

Write-Host 'Building Build.Dockerfile'
docker build --build-arg HTTP_PROXY=%HTTP_PROXY% --build-arg HTTPS_PROXY=%HTTPS_PROXY% -f Build.Dockerfile -t dotnet471-build .

# Required to create a writeable container for the build
Write-Host 'Creating temporary container'
docker create --name dotnet471-build-temp dotnet471-build

Write-Host 'Copying C:\\build\\'$ProjectName'\\bin'
docker cp dotnet471-build-temp:C:\\build\\$ProjectName\\bin .\\$ProjectName\bin
Write-Host 'Removing dotnet471-build-temp'
docker rm dotnet471-build-temp

Write-Host 'Building Dockerfile'
docker build -f Dockerfile -t $ProjectName .
