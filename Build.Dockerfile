FROM microsoft/dotnet-framework-build

SHELL ["powershell"]

COPY . 'C:\\build\\'
WORKDIR 'C:\\build\\'

RUN ["nuget.exe", "restore"]
RUN ["msbuild.exe", "C:\\build\\dotnet471api.sln", "/p:Configuration=Release"]

CMD ["powershell"]