# Dot Net 4.7.1 Build in a container

## Requirements

* Encapsulate the build process inside a container to isolate the dependencies

## Overview

* Windows does not support docker in docker
* The approach implemented is building inside a container from the image Build.Docker file
* The runable image is build on the host using the Dockerfile
* Build.ps1 executes all the steps required to produce the runnable docker image

## Jenkins on Windows Setup

0. Install Jenkins and Docker on Windows Server 2016
1. Create a new Jenkins job
2. Point the source to a git repo
3. In the build step run .\Build.ps1
4. Run the job and check for any errors in the console output

## How to run the resulting container

1. The image is built on the host
2. Use ```docker images``` to find the newly built image
3. Run the image using:
	```
	docker run --rm -d -t -p 80:80 dotnet471api
	```
4. To access the image, find the container id using ```docker ps```
5. Find the container ip using ```docker inspect {containerId}```
6. Open a browser to the container ip and you should see default asp.net page
7. Please note: localhost or host ip address WON'T WORK to access the container

## What to adjust when using on another project

1. Build.ps1 should list name of the new project/solution
2. Dockerfile should list name of the new project
3. Build.Dockerfile should list name of the new project